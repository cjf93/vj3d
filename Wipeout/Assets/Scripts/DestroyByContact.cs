﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
	public GameObject explosion;
	public GameObject playerExplosion;
    AudioSource AudioToPlay;

    public delegate void PlayerFrenada(bool b);
    public static event PlayerFrenada onPlayerhit;

    void Start()
    {
        AudioToPlay = GameObject.Find("explosion").GetComponent<AudioSource>();
    }

	void OnTriggerEnter(Collider other) 
	{
        Debug.Log(other.tag);
		if (other.tag == "Player")
		{
            //	Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            //gameController.GameOver ();
            Instantiate(explosion, transform.position, transform.rotation);
            onPlayerhit(true);
            AudioToPlay.Play();
            Destroy(gameObject);
        }
	}
    void OnTriggerExit(Collider other)
    {
        Debug.Log(other.tag);
        if (other.tag == "Terrain")
        {
            //	Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            //gameController.GameOver ();
            Instantiate(explosion, transform.position, transform.rotation);
            AudioToPlay.Play();
            Destroy(gameObject);
        }
    }
}