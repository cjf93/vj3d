﻿using UnityEngine;
using System.Collections;

public class DestroyDelayed : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(DestroyD());
	}
	IEnumerator DestroyD()
    {
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }
	// Update is called once per frame
	void Update () {
	
	}
}
