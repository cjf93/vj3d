﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IA_V1 : MonoBehaviour {

	public float acceleration = 5f;
	public float deceleracion = -3f;
	public float maxSpeed = 1.5f;
	public float friction = -1f;
	public float giro = 1f;
	public float gForce = 0f;
	public float minDistanceRight = 1f;
	public float minDistanceLeft = 1f;
	public float factorInterpolacionDireccion = 0.1f; // valor interpolar el forward
	public float factorInterpolacionAltura = 0.05f; // para la altura, suavizar.
	public LayerMask raycastWith;
	public float coeficienteRocePared=0.6f;
	public float tiempoReacion=1f;
	public float tiempoTranscurrido=0;

	public Text DebugLabel;

	float speed = 0;
	bool accelerando;
	bool frenando; 
	bool girando;
	private bool colisonPowerUp=false;


	Vector3 actualRotation;
	Vector3 normalTerrain;
	Vector3 normalWall;
	Vector3 myForward;
	Vector3 direccion;
	float distanceDown;
	float distanceRight;
	float distanceLeft;
	bool canMove = false;
	// Use this for initialization
	void Start () {
		UIController.onPlayerCanMove += setCanMove;
	}
    void onDesroy()
    {
        UIController.onPlayerCanMove -= setCanMove;
    }
	// Update is called once per frame
	void Update () {
		tiempoTranscurrido += Time.deltaTime;
		if (canMove) {
			if (tiempoTranscurrido > tiempoReacion) {
				tomarDecision ();
				tiempoTranscurrido = 0;
			}
			recalculatePosition ();
			alignNormals ();
			HandleMovement ();
			recalculatePosition ();
		}
	}
	void FixedUpdate ()
	{

	}
	void tomarDecision()
	{
		girando = false;
		frenando = false;
		actualRotation = this.transform.localEulerAngles;
		float anchoPista = doRaycastingSideLeft () + doRaycastingSideRight ();
		float dist = doRaycastingSideLeft () / anchoPista;
		accelerando = true;
		if (dist>0.55)
		{
			Debug.Log ("cerca derecha");
			if (doRaycastingSideLeft () - giro > minDistanceLeft)
			{
				Debug.Log ("giro derecha");
				this.transform.Translate(Vector3.left * giro*Time.deltaTime*30);
			}
		}
		if (dist<0.45)
		{
			Debug.Log ("cerca izquierda");
			girando = true;
			if (doRaycastingSideRight () - giro > minDistanceRight)
				Debug.Log ("giro izquierda");
				this.transform.Translate(Vector3.right * giro*Time.deltaTime*30);
		}
	}
		

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "PowerUp") {
			colisonPowerUp = true;
		}
	}

	void HandleMovement()
	{
		distanceRight = doRaycastingSideRight();
		distanceLeft = doRaycastingSideLeft();
		myForward = this.transform.forward;
		float colisionPared = 0;
		if (distanceRight < minDistanceRight || distanceLeft < minDistanceLeft)
		{
			colisionPared = coeficienteRocePared;
		}

		if (accelerando) speed += (acceleration-colisionPared) * Time.deltaTime;
		else if (frenando) speed += (deceleracion-colisionPared) * Time.deltaTime;
		else speed += (friction-colisionPared) * Time.deltaTime;
		if (speed < 0) speed = 0;
		else if (speed > maxSpeed) speed = maxSpeed;
		if (colisonPowerUp) {
			speed = speed * 0.3f;
			colisonPowerUp = false;
		}

		Vector3 tempPos = this.transform.position;
		tempPos = tempPos + Vector3.forward * speed;
		this.transform.Translate(Vector3.forward * speed*Time.deltaTime*30);

	}
	void alignNormals()
	{
		Vector3 lastDireccion = this.transform.forward;
		direccion = Vector3.Cross(normalTerrain, normalWall);
		direccion = Vector3.Lerp(lastDireccion, direccion, factorInterpolacionDireccion);
		this.transform.forward = direccion;


		float f = Vector3.Angle(direccion, lastDireccion) * Mathf.Sign(Vector3.Cross(direccion, lastDireccion).y);
		if (f > 0)
		{
			this.transform.Translate(Vector3.right *(speed / 10) * gForce*Time.deltaTime*30);
		}
		if (f < -1)
		{
			this.transform.Translate(Vector3.right * (speed / 10) * -gForce*Time.deltaTime*30);
		}
	}
	void recalculatePosition()
	{
		distanceDown = doRaycastingDown();
		distanceRight = doRaycastingSideRight();
		distanceLeft = doRaycastingSideLeft();
		//Todo reposicionar
		if (distanceRight < minDistanceRight)
		{
			this.transform.Translate(Vector3.left*Time.deltaTime*30 * (minDistanceRight-distanceRight));
		}
		if (distanceLeft < minDistanceLeft)
		{
			this.transform.Translate(Vector3.right*Time.deltaTime*30 * (minDistanceLeft-distanceLeft));
		}
		if (distanceDown < 0.5f && distanceDown>0)
		{
			this.transform.Translate(Vector3.up*Time.deltaTime*30 * factorInterpolacionAltura / distanceDown);
		}
		if (distanceDown > 0.8f)
		{
			this.transform.Translate(Vector3.up*Time.deltaTime*30 * -factorInterpolacionAltura * distanceDown);
		}
	}
	float doRaycastingDown()
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position, Vector3.down, out hit, 50f, raycastWith))
		{
			Debug.DrawRay(this.transform.position, hit.normal * 50, Color.red);
			normalTerrain = hit.normal;
		}
		return hit.distance;
	}
	float doRaycastingSideRight()
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position, this.transform.TransformDirection(1, 0, 0), out hit, 50f))
		{
			Debug.DrawRay(this.transform.position, hit.normal * 50, Color.cyan);
			normalWall = hit.normal;
		}
		return hit.distance;
	}
	float doRaycastingSideLeft()
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position, this.transform.TransformDirection(-1,0,0), out hit, 50f))
		{
			Debug.DrawRay(this.transform.position, hit.normal * 50, Color.red);
		}
		return hit.distance;
	}
	public float getSpeed()
	{
		return speed;
	}
	public void setCanMove(bool b)
	{
		canMove = b;
	}
}