﻿using UnityEngine;
using System.Collections;

public class PowerUpController_IA : MonoBehaviour {

	public float time=5000f;
	public float currentTime= 5000f;
	public GameObject[] powerUps;
	public LayerMask raycastWith;



	private int seleccionado;
	private bool activado = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!activado){
				currentTime -= Time.deltaTime;
			if (currentTime <= 0f) {
				System.Random rnd = new System.Random ();
				seleccionado = rnd.Next (powerUps.Length);
				Debug.Log ("activadoPowerUp");
				currentTime = time;
				activado = true;
			}
		}
		else shoot();

	}

	void shoot()
	{
		if (activado) {
			Vector3 posicion = new Vector3 (0, 0, 0);
			Quaternion rotation = new Quaternion ();
			switch (powerUps [seleccionado].name) {
			case "misil":
				posicion = this.transform.position + Vector3.Normalize (this.transform.forward) * 5f;
				rotation = this.transform.rotation * Quaternion.Euler (0, 180f, 0);
					Instantiate (powerUps [seleccionado], posicion, rotation);
				    Debug.Log ("Tengo el cubo detras");
				activado = false;
				break;
			case "mina":
				posicion = this.transform.position + Vector3.Normalize (this.transform.forward) * -2f;
				rotation = this.transform.rotation;
					Debug.Log ("Tengo el cubo delante");
					Instantiate (powerUps [seleccionado], posicion, rotation);
					activado = false;
				break;
			default:
				break;
			}

	
		}
	}
}
