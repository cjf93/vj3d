﻿using UnityEngine;
using System.Collections;

public enum Modo { ModoContrareloj, ModoVsIa};
public enum Nave { Azul, Roja};
public class GameController : MonoBehaviour {

    public static GameController instance = null;
    public Modo modoDeJuego;
    public Nave naveSeleccionada;

    void Awake()
    {
        //--Singleton pattern --//
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);
        //--Singleton pattern --//
        DontDestroyOnLoad(this);
    }
    // Seleccionamos unos por defecto
    void Start () {
        naveSeleccionada = Nave.Azul;
        modoDeJuego = Modo.ModoContrareloj;
	}
	
    public Modo getMododeJuego()
    {
        return modoDeJuego;
    }
    public Nave getNave()
    {
        return naveSeleccionada;
    }
    public void setModoDeJuego(int i)
    {
        if (i == 0)
        {
            modoDeJuego = Modo.ModoContrareloj;
        }
        else if (i == 1) modoDeJuego = Modo.ModoVsIa;
    }
    public void setNaveSeleccionada(int i)
    {
        if (i == 0) naveSeleccionada = Nave.Azul;
        else if (i == 1) naveSeleccionada = Nave.Roja;
    }
}
