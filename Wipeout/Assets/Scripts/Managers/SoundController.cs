﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundController : MonoBehaviour {

    public static SoundController instance = null; 

    public AudioSource[] FxSounds;
    public AudioSource[] MusicSounds;

    int currentAudio;
    // Use this for initialization
    void Awake()
    {
        //--Singleton pattern --//
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);
        //--Singleton pattern --//
        DontDestroyOnLoad(this);
        MusicSliderController.onVolumeChange += SetVolumeToMusic;
        FxSliderController.onVolumeChange += SetVolumeToFx;
    }
    void Start () {
        MusicSounds[0].Play();
        currentAudio = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (!MusicSounds[currentAudio].isPlaying)
        {
            ++currentAudio;
            if (currentAudio >= MusicSounds.Length) currentAudio = 0;
            MusicSounds[currentAudio].Play();
        }
	}
    void Destroy()
    {
        MusicSliderController.onVolumeChange -= SetVolumeToMusic;
        FxSliderController.onVolumeChange -= SetVolumeToFx;
    }
    void SetVolumeToMusic(float volume)
    {
        PlayerPrefs.SetFloat("MusicVolume", volume);
        foreach(AudioSource a in MusicSounds)
        {
            a.volume = (volume/100);
        }
    }
    void SetVolumeToFx(float volume)
    {
        PlayerPrefs.SetFloat("FXVolume", volume);
        foreach (AudioSource a in FxSounds)
        {
            a.volume = (volume/100);
        }
    }
}
