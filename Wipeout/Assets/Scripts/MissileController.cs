﻿using UnityEngine;
using System.Collections;

public class MissileController : MonoBehaviour {

	public float speed;
	public GameObject explosion;
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		this.transform.Translate(Vector3.forward * -speed);
	}
}
