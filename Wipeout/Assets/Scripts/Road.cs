﻿using System;
using UnityEngine;
using vj;

namespace vj
{
	public class Road
	{
		private Vector3 entrada;
		private Vector3 salida;
		private int id;
		private direccion direc_entrada;
		private direccion direc_salida;

		public Road ()
		{
		}

		public Road(Vector3 entrada, Vector3 salida, int id, direccion direc_entrada, direccion direc_salida){
			this.entrada = entrada;
			this.salida = salida;
			this.id = id;
			this.direc_entrada = direc_entrada;
			this.direc_salida = direc_salida;
		}

		public Vector3 Entrada {
			get{ return entrada; }
		}
		public Vector3 Salida {
			get{ return salida; }
		}
		public int Id{
			get{return id;}
		}
		public direccion Direc_salida{
			get{ return direc_salida;}
		}
		public direccion Direc_entrada{
			get{ return direc_entrada;}
		}
	}
}
