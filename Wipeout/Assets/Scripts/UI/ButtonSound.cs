﻿using UnityEngine;
using System.Collections;

public class ButtonSound : MonoBehaviour {

    public string Audioname;
    AudioSource AudioToPlay;
	// Use this for initialization
	void Start () {
        AudioToPlay = GameObject.Find(Audioname).GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void PlaySound(){
        AudioToPlay.Play();
    }
}
