﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FxSliderController : MonoBehaviour {

    public delegate void FxVolumeChange(float volume);
    public static event FxVolumeChange onVolumeChange;

    public Slider FxSlider;
    public Text FxValueText;

	// Use this for initialization
	void Start () {
        float f = PlayerPrefs.GetFloat("FXVolume", 100);
        FxValueText.text = f.ToString();
        FxSlider.value = f;
        onVolumeChange(f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void SetFxText()
    {
        FxValueText.text = FxSlider.value.ToString();
        onVolumeChange(FxSlider.value);
    }

}
