﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections;

public class MenuController : MonoBehaviour {

    public CanvasGroup[] Screens;
    public Tweener[] ScreensAnims;
    public Tweener KeyboardInstructionsAnim;
    public Tweener GamepadInstructionsAnim;
    public Text HighScoreTextSolo;
    public Text HighScoreTextVsIA;

    public Button PlayButton;
    public Button PlayModeVsIAButton;
    public Button PlayTimeAttackButton;
    public Button InstructionsButton;
    public Button KeyboardInstructionsButton;
    public Button GamepadInstructionsButton;
    public Button OptionsButton;
    public Button CreditsButton;
    public Button ExitButton;
    public Button BackButton;
    public GameObject g_BackButton;

    public delegate void ButtonClick();
    public ButtonClick OnPlayButtonClick;
    public ButtonClick OnPlayModeVsIAButtonClick;
    public ButtonClick OnPlayTimeAttackButtonClick;
    public ButtonClick OnInstructionsButtonClick;
    public ButtonClick OnKeyboardInstructionsButtonClick;
    public ButtonClick OnGamepadInstructionsButtonClick;
    public ButtonClick OnOptionsButtonClick;
    public ButtonClick OnCreditsButtonClick;
    public ButtonClick OnExitButtonClick;
    public ButtonClick OnBackButtonClick;

    // Use this for initialization
    void Start () {
        MapButtonListeners();
        BindDelegates();
        ShowFadeMenuScreen();
		HighScoreTextVsIA.text  = "HighScore: "+MinutesToString(PlayerPrefs.GetFloat("TiempoSolo", 0))+":"+ SecondsToString(PlayerPrefs.GetFloat("TiempoSolo", 0)) + ":"+ MilisecondsToString(PlayerPrefs.GetFloat("TiempoSolo", 0));
		HighScoreTextSolo.text  = "HighScore: "+MinutesToString(PlayerPrefs.GetFloat("TiempoVsIA", 0)) + ":" + SecondsToString(PlayerPrefs.GetFloat("TiempoVsIA", 0)) + ":" + MilisecondsToString(PlayerPrefs.GetFloat("TiempoVsIA", 0));
    }
    void OnDestroy()
    {
        UnBindDelegates();
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            ShowExitScreen();
        }
	}

    private void MapButtonListeners()
    {
        // Header button
        PlayButton.onClick.AddListener(() =>
        {
            OnPlayButtonClick();
        });
        PlayModeVsIAButton.onClick.AddListener(() =>
        {
            OnPlayModeVsIAButtonClick();
        });
        PlayTimeAttackButton.onClick.AddListener(() =>
        {
            OnPlayTimeAttackButtonClick();
        });

        InstructionsButton.onClick.AddListener(() =>
        {
            OnInstructionsButtonClick();
        });
        KeyboardInstructionsButton.onClick.AddListener(() =>
        {
            OnKeyboardInstructionsButtonClick();
        });
        GamepadInstructionsButton.onClick.AddListener(() =>
        {
            OnGamepadInstructionsButtonClick();
        });

        OptionsButton.onClick.AddListener(() =>
        {
            OnOptionsButtonClick();
        });

        ExitButton.onClick.AddListener(() =>
        {
            OnExitButtonClick();
        });

        CreditsButton.onClick.AddListener(() =>
        {
            OnCreditsButtonClick();
        });
        BackButton.onClick.AddListener(() =>
        {
            OnBackButtonClick();
        });
    }
    // BIND (+)
    private void BindDelegates()
    {
        OnPlayButtonClick += ShowSelectModeScreen;
        OnPlayButtonClick += HideMenuDissapearLeft;

        OnInstructionsButtonClick += ShowIntructionsScreen;
        OnInstructionsButtonClick += HideMenuDissapearLeft;
        OnKeyboardInstructionsButtonClick += ShowKeyboardInstructions;
        OnGamepadInstructionsButtonClick += ShowGamePadInstructions;

        OnOptionsButtonClick += ShowOptionsScreen;
        OnOptionsButtonClick += HideMenuDissapearLeft;

        OnCreditsButtonClick += ShowCreditsSCreen;
        OnCreditsButtonClick += HideMenuDissapearLeft;

        OnExitButtonClick += ShowExitScreen;
        OnPlayModeVsIAButtonClick += GoVsIAMode;
        OnPlayTimeAttackButtonClick += GoTimeAttackMode;
    }
    // UNBIND (-)
    private void UnBindDelegates()
    {
        OnPlayButtonClick -= ShowSelectModeScreen;
        OnPlayButtonClick -= HideSelectModeScreen;
        OnInstructionsButtonClick -= ShowIntructionsScreen;
        OnInstructionsButtonClick -= HideMenuDissapearLeft;
        OnKeyboardInstructionsButtonClick -= ShowKeyboardInstructions;
        OnGamepadInstructionsButtonClick -= ShowGamePadInstructions;
        OnOptionsButtonClick -= ShowOptionsScreen;
        OnOptionsButtonClick -= HideMenuDissapearLeft;
        OnCreditsButtonClick -= ShowCreditsSCreen;
        OnCreditsButtonClick -= HideMenuDissapearLeft;
        OnExitButtonClick -= ShowExitScreen;
        OnPlayModeVsIAButtonClick -= GoVsIAMode;
        OnPlayTimeAttackButtonClick -= GoTimeAttackMode;
    }

    void ShowFadeMenuScreen()
    {
        ScreensAnims[0].Play("FadeIn");
    }
    void ShowMenuAppearLeft()
    {
        g_BackButton.SetActive(false);
        ScreensAnims[0].Play("AppearLeft");
    }
    void HideMenuDissapearLeft()
    {
        ScreensAnims[0].Play("DissapearLeft");
        g_BackButton.SetActive(true);
    }
    void ShowSelectModeScreen()
    {
        OnBackButtonClick += HideSelectModeScreen;
        OnBackButtonClick += ShowMenuAppearLeft;
        ScreensAnims[4].Play("AppearLeft");
    }
    void HideSelectModeScreen()
    {
        OnBackButtonClick -= HideSelectModeScreen;
        OnBackButtonClick -= ShowMenuAppearLeft;
        ScreensAnims[4].Play("DissapearRight");
    }
    void ShowIntructionsScreen()
    {
        OnBackButtonClick += HideIntructionsScreen;
        OnBackButtonClick += ShowMenuAppearLeft;
        ScreensAnims[1].Play("AppearLeft");
    }
    void HideIntructionsScreen()
    {
        OnBackButtonClick -= HideIntructionsScreen;
        OnBackButtonClick -= ShowMenuAppearLeft;
        ScreensAnims[1].Play("DissapearRight");
    }
    void ShowKeyboardInstructions()
    {
        GamepadInstructionsAnim.Play("FadeOut");
        KeyboardInstructionsAnim.Play("FadeIn");
    }
    void ShowGamePadInstructions()
    {
        KeyboardInstructionsAnim.Play("FadeOut");
        GamepadInstructionsAnim.Play("FadeIn");
    }
    void ShowOptionsScreen()
    {
        OnBackButtonClick += HideOptionsScreen;
        OnBackButtonClick += ShowMenuAppearLeft;
        ScreensAnims[2].Play("AppearLeft");
    }
    void HideOptionsScreen()
    {
        OnBackButtonClick -= HideOptionsScreen;
        OnBackButtonClick -= ShowMenuAppearLeft;
        ScreensAnims[2].Play("DissapearRight");
    }
    void ShowCreditsSCreen()
    {
        OnBackButtonClick += HideCreditsScreen;
        OnBackButtonClick += ShowMenuAppearLeft;
        ScreensAnims[5].Play("AppearLeft");
    }
    void HideCreditsScreen()
    {
        OnBackButtonClick -= HideCreditsScreen;
        OnBackButtonClick += ShowMenuAppearLeft;
        ScreensAnims[5].Play("DissapearRight");
    }
    void GoVsIAMode()
    {
        GameController.instance.setModoDeJuego(0);
		SceneManager.LoadSceneAsync("01_TimeAttack_V2");
    }
    void GoTimeAttackMode()
    {
        GameController.instance.setModoDeJuego(1);
        SceneManager.LoadSceneAsync("01_TimeAttack");
    }
    void GoRandomMode()
    {
        SceneManager.LoadSceneAsync("01_TimeAttack");
    }
    void ShowExitScreen()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
    public void SelectNave(int i)
    {
        GameController.instance.setNaveSeleccionada(i);
    }
    string MilisecondsToString(float f)
    {
        string s_miliseconds = "00";
        int miliseconds, seconds = 0;
        float f_miliseconds;
        seconds = ((int)f);
        f_miliseconds = (f - seconds);
        f_miliseconds = f_miliseconds * 100;
        miliseconds = (int)f_miliseconds;

        if (miliseconds < 10)
        {
            s_miliseconds = miliseconds + "00";
        }
        if (miliseconds > 10 && miliseconds < 100)
        {
            s_miliseconds = miliseconds + "0";
        }
        return s_miliseconds;
    }
    string SecondsToString(float f)
    {
        string s_seconds = "00";
        int seconds, minutes = 0;
        seconds = ((int)f);
        minutes = seconds / 60;

        seconds = seconds - minutes * 60;
        s_seconds = seconds.ToString();
        if (seconds < 10)
        {
            s_seconds = "0" + seconds;
        }
        return s_seconds;

    }
    string MinutesToString(float f)
    {
        string s_minutes = "00";
        int seconds, minutes = 0;
        seconds = ((int)f);
        minutes = seconds / 60;

        if (minutes < 10)
        {
            s_minutes = "0" + minutes;
        }
        return s_minutes;
    }

}
