﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MusicSliderController : MonoBehaviour {

    public delegate void VolumeChange(float volume);
    public static event VolumeChange onVolumeChange;

    public Slider MusicSlider;
    public Text MusicValueText;

	// Use this for initialization
	void Start () {
        float f = PlayerPrefs.GetFloat("MusicVolume", 100);
        MusicValueText.text = f.ToString();
        MusicSlider.value = f;
        onVolumeChange(f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetMusicText()
    {
        MusicValueText.text = MusicSlider.value.ToString();
        onVolumeChange(MusicSlider.value);
    }
}
