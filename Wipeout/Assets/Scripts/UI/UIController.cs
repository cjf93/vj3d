﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UIController : MonoBehaviour {

    public Animator CameraEnterAnim;
    GameObject cubeCamera;
    public GameObject animCamera;
    public GameObject naveAzul;
    public GameObject naveRoja;
    public Animator Countdown;
	public Text[] TimerLabels;
    public Text[] LapTimerLabels;
    public Text[] CheckPointTimerLabels;
    public Text[] CheckPointDiffTimerLabels;
    public Text SpeedLabel;
    public Text LapCounterLabel;
    public Text[] YourTimeLabel;
    public Text[] YourHighScoreLabel;
    public Tweener HighScoreAnim;
    public GameObject SpeedBar;
    public GameObject SpeedBarFinalPosition;
    public Tweener PanelCheckPointsAnim;
    public Text DebugText;

    int LapCounter = 0;
    int TotalLaps = 3;
    int CheckPointNumber = 3;
    float[] TimeOnCheckPoint = new float[3] {0,0,0};
    float[] LastTimeOnCheckPoint = new float[3] { 0, 0, 0 };
    float BestLapTime = 0;

	bool LapStarted;
	float StartTime;
    float SpeedBarInitPos;
    float movientoPorMPH;
    bool startAnimFinished = false;
    bool playerCanMove = false;
    bool gameFinished = false;
    string playerPrefHighScore;
    CubeMovement player;

    public delegate void PlayerMovement(bool b);
    public static event PlayerMovement onPlayerCanMove;

    // Use this for initialization
    void Start()
    {
        LapStarted = false;
        SpeedBarInitPos = SpeedBar.transform.position.x;
        movientoPorMPH = (Mathf.Abs(SpeedBarFinalPosition.transform.position.x) - Mathf.Abs(SpeedBarInitPos)) / 120f;
        CubeMovement.OnCheckPoint += HandleCheckPoint;
        if (GameController.instance.getMododeJuego() == Modo.ModoContrareloj) playerPrefHighScore = "TiempoSolo";
        else if (GameController.instance.getMododeJuego() == Modo.ModoVsIa) playerPrefHighScore = "TiempoVsIA";
        if (GameController.instance.getNave() == Nave.Azul)
        {
            naveRoja.SetActive(false);
            player = GameObject.Find("NaveBlue").GetComponent<CubeMovement>();
            cubeCamera = GameObject.Find("NaveBlue");
        }
        else if (GameController.instance.getNave() == Nave.Roja) {
            naveAzul.SetActive(false);
            player = GameObject.Find("NaveRed").GetComponent<CubeMovement>();
            cubeCamera = GameObject.Find("NaveRed");
        }
    }
	void OnDestroy() {
        CubeMovement.OnCheckPoint -= HandleCheckPoint;
    }
	// Update is called once per frame
	void Update () {
        SpeedLabel.text = Mathf.Round(player.getSpeed()*80) + "MPH";
        SpeedBar.transform.position = new Vector3(SpeedBarInitPos + (Mathf.Round(player.getSpeed() * 80) * movientoPorMPH), SpeedBar.transform.position.y, SpeedBar.transform.position.z);
        if (LapStarted && !gameFinished)
        {
            TimerLabels[0].text = MilisecondsToString(Time.time - StartTime);
            TimerLabels[1].text = SecondsToString(Time.time - StartTime);
            TimerLabels[2].text = MinutesToString(Time.time - StartTime);
        }
        if (!startAnimFinished)
        {
            if (CameraEnterAnim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                startAnimFinished = true;
                Countdown.enabled = true;
                cubeCamera.GetComponentInChildren<Camera>().enabled = true;
                StartCoroutine(Wait3Sec());
}
        }
	}
    string MilisecondsToString(float f)
    {
        string s_miliseconds = "00";
        int miliseconds, seconds = 0;
        float f_miliseconds;
        seconds = ((int)f);
        f_miliseconds = (f - seconds);
        f_miliseconds = f_miliseconds * 100;
        miliseconds = (int)f_miliseconds;

        if (miliseconds < 10)
        {
            s_miliseconds = miliseconds + "00";
        }
        if (miliseconds > 10 && miliseconds < 100)
        {
            s_miliseconds = miliseconds+"0";
        }
        return s_miliseconds;
    }
    string SecondsToString(float f)
    {
        string s_seconds = "00";
        int seconds, minutes = 0;
        seconds = ((int)f);
        minutes = seconds / 60;

        seconds = seconds - minutes * 60;
        s_seconds = seconds.ToString();
        if (seconds < 10)
        {
            s_seconds = "0" + seconds;
        }
        return s_seconds;

    }
    string MinutesToString(float f)
    {
        string s_minutes = "00";
        int  seconds, minutes = 0;
        seconds = ((int)f);
        minutes = seconds / 60;

        if (minutes < 10)
        {
            s_minutes = "0" + minutes;
        }
        return s_minutes;
    }
   IEnumerator Wait3Sec()
    {
        yield return new WaitForSeconds(3f);
        playerCanMove = true;
        onPlayerCanMove(true);
    }
    void HandleCheckPoint(int n)
    {
  
        Debug.Log("checkpoint: "+n);
        switch (n)
        {
            case 0:
                float TimeOnEnter0 = Time.time;
                if(LapStarted == false)
                {
                    LapStarted = true;
                    StartTime = TimeOnEnter0;
                    LastTimeOnCheckPoint[0] = StartTime;
                }
                ++LapCounter;
                if (LapCounter > 1)
                {
                   if(BestLapTime > (TimeOnEnter0 - StartTime)) BestLapTime = TimeOnEnter0 - StartTime;
                    LapTimerLabels[0].text = MilisecondsToString(TimeOnEnter0 - StartTime);
                    LapTimerLabels[1].text = SecondsToString(TimeOnEnter0 - StartTime);
                    LapTimerLabels[2].text = MinutesToString(TimeOnEnter0 - StartTime);
                    TimeOnCheckPoint[0] = TimeOnEnter0 - TimeOnCheckPoint[2];
                    LastTimeOnCheckPoint[0] = TimeOnEnter0;
                }
                if (LapCounter > TotalLaps)
                {
                    gameFinished = true;
                    //deshabilitar controller
                    onPlayerCanMove(false);
                    //parar tiempo, guardar highscore
                    YourTimeLabel[0].text = MilisecondsToString(TimeOnEnter0 - StartTime);
                    YourTimeLabel[1].text = SecondsToString(TimeOnEnter0 - StartTime);
                    YourTimeLabel[2].text = MinutesToString(TimeOnEnter0 - StartTime);

                    if(PlayerPrefs.GetFloat(playerPrefHighScore, 0) == 0)
                    {
                        Debug.Log("Hola");
                        PlayerPrefs.SetFloat(playerPrefHighScore, TimeOnEnter0 - StartTime);
                        YourHighScoreLabel[0].text = MilisecondsToString(TimeOnEnter0 - StartTime);
                        YourHighScoreLabel[1].text = SecondsToString(TimeOnEnter0 - StartTime);
                        YourHighScoreLabel[2].text = MinutesToString(TimeOnEnter0 - StartTime);
                    }
                    else
                    {
                        if(PlayerPrefs.GetFloat(playerPrefHighScore) > (TimeOnEnter0 - StartTime))
                        {
                            PlayerPrefs.SetFloat(playerPrefHighScore, TimeOnEnter0 - StartTime);
                            YourHighScoreLabel[0].text = MilisecondsToString(TimeOnEnter0 - StartTime);
                            YourHighScoreLabel[1].text = SecondsToString(TimeOnEnter0 - StartTime);
                            YourHighScoreLabel[2].text = MinutesToString(TimeOnEnter0 - StartTime);
                        }
                        else
                        {
                            YourHighScoreLabel[0].text = MilisecondsToString(PlayerPrefs.GetFloat(playerPrefHighScore));
                            YourHighScoreLabel[1].text = SecondsToString(PlayerPrefs.GetFloat(playerPrefHighScore));
                            YourHighScoreLabel[2].text = MinutesToString(PlayerPrefs.GetFloat(playerPrefHighScore));
                        }
                    }
                    Debug.Log(PlayerPrefs.GetFloat(playerPrefHighScore, 0));
                    //activar animacion highscores
                    HighScoreAnim.Play("FadeIn");
                    //Esperar X segundos y enviar al menu.
                }
                LapCounterLabel.text = "Lap:  " + LapCounter + "/" + TotalLaps;
                break;
            case 1:
                float TimeOnEnter1 = Time.time;
                CheckPointTimerLabels[0].text = MilisecondsToString(TimeOnEnter1 - LastTimeOnCheckPoint[0]);
                CheckPointTimerLabels[1].text = SecondsToString(TimeOnEnter1 - LastTimeOnCheckPoint[0]);
                CheckPointTimerLabels[2].text = MinutesToString(TimeOnEnter1 - LastTimeOnCheckPoint[0]);

                CheckPointDiffTimerLabels[0].text = MilisecondsToString(Mathf.Abs((TimeOnEnter1 - LastTimeOnCheckPoint[0]) - TimeOnCheckPoint[1]));
                CheckPointDiffTimerLabels[1].text = SecondsToString(Mathf.Abs((TimeOnEnter1 - LastTimeOnCheckPoint[0]) - TimeOnCheckPoint[1]));
                CheckPointDiffTimerLabels[2].text = MinutesToString(Mathf.Abs((TimeOnEnter1 - LastTimeOnCheckPoint[0]) - TimeOnCheckPoint[1]));
                if (((TimeOnEnter1 - LastTimeOnCheckPoint[0]) < TimeOnCheckPoint[1]))
                {
                    CheckPointDiffTimerLabels[0].color = Color.green;
                    CheckPointDiffTimerLabels[1].color = Color.green;
                    CheckPointDiffTimerLabels[2].color = Color.green;
                    CheckPointDiffTimerLabels[3].color = Color.green;
                    CheckPointDiffTimerLabels[4].color = Color.green;
                    CheckPointDiffTimerLabels[5].color = Color.green;
                    CheckPointDiffTimerLabels[3].text = "-";
                }
                else
                {
                    CheckPointDiffTimerLabels[0].color = Color.red;
                    CheckPointDiffTimerLabels[1].color = Color.red;
                    CheckPointDiffTimerLabels[2].color = Color.red;
                    CheckPointDiffTimerLabels[3].color = Color.red;
                    CheckPointDiffTimerLabels[4].color = Color.red;
                    CheckPointDiffTimerLabels[5].color = Color.red;
                    CheckPointDiffTimerLabels[3].text = "+";
                }
                PanelCheckPointsAnim.Play("FadeInAndOut");
                TimeOnCheckPoint[1] = (TimeOnEnter1 - LastTimeOnCheckPoint[0]);
                LastTimeOnCheckPoint[1] = TimeOnEnter1;
                break;
            case 2:
                float TimeOnEnter2 = Time.time;
                CheckPointTimerLabels[0].text = MilisecondsToString(TimeOnEnter2 - LastTimeOnCheckPoint[1]);
                CheckPointTimerLabels[1].text = SecondsToString(TimeOnEnter2 - LastTimeOnCheckPoint[1]);
                CheckPointTimerLabels[2].text = MinutesToString(TimeOnEnter2 - LastTimeOnCheckPoint[1]);

                CheckPointDiffTimerLabels[0].text = MilisecondsToString(Mathf.Abs((TimeOnEnter2 - LastTimeOnCheckPoint[1]) - TimeOnCheckPoint[2]));
                CheckPointDiffTimerLabels[1].text = SecondsToString(Mathf.Abs((TimeOnEnter2 - LastTimeOnCheckPoint[1]) - TimeOnCheckPoint[2]));
                CheckPointDiffTimerLabels[2].text = MinutesToString(Mathf.Abs((TimeOnEnter2 - LastTimeOnCheckPoint[1]) - TimeOnCheckPoint[2]));

                if (((TimeOnEnter2 - LastTimeOnCheckPoint[1]) < TimeOnCheckPoint[2]))
                {
                    CheckPointDiffTimerLabels[0].color = Color.green;
                    CheckPointDiffTimerLabels[1].color = Color.green;
                    CheckPointDiffTimerLabels[2].color = Color.green;
                    CheckPointDiffTimerLabels[3].color = Color.green;
                    CheckPointDiffTimerLabels[4].color = Color.green;
                    CheckPointDiffTimerLabels[5].color = Color.green;
                    CheckPointDiffTimerLabels[3].text = "-";
                }
                else
                {
                    CheckPointDiffTimerLabels[0].color = Color.red;
                    CheckPointDiffTimerLabels[1].color = Color.red;
                    CheckPointDiffTimerLabels[2].color = Color.red;
                    CheckPointDiffTimerLabels[3].color = Color.red;
                    CheckPointDiffTimerLabels[4].color = Color.red;
                    CheckPointDiffTimerLabels[5].color = Color.red;
                    CheckPointDiffTimerLabels[3].text = "+";
                }
                PanelCheckPointsAnim.Play("FadeInAndOut");
                TimeOnCheckPoint[2] = (TimeOnEnter2 - LastTimeOnCheckPoint[1]);
                LastTimeOnCheckPoint[2] = TimeOnEnter2;
                break;
        }

    }
    public void ReplayScene()
    {
        if (GameController.instance.getMododeJuego() == Modo.ModoContrareloj)
        {
            SceneManager.LoadSceneAsync("01_TimeAttack");
        }
        if (GameController.instance.getMododeJuego() == Modo.ModoVsIa)
        {
            SceneManager.LoadSceneAsync("01_TimeAttack V2");
        }

    }
    public void GoToMenu()
    {
        SceneManager.LoadScene("00_MainMenu");
    }
}
