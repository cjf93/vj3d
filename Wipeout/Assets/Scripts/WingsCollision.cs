﻿using UnityEngine;
using System.Collections;

public class WingsCollision : MonoBehaviour
{


    public delegate void PlayerFrenada2(bool b);
    public static event PlayerFrenada2 onPlayerhit;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            onPlayerhit(true);
        }
    }
}
