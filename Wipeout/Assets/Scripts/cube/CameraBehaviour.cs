﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour {

    public CubeMovement cubeMovement_instance;
    public float desplazamientoCamara;
    Vector3 pos;
    Vector3 tempPos;
	// Use this for initialization
	void Start () {
        pos = this.transform.localPosition;
        tempPos = this.transform.localPosition;
    }
	
	// Update is called once per frame
	void Update () {
        float speed = cubeMovement_instance.getSpeed();
        tempPos.z = pos.z - speed* desplazamientoCamara;
        this.transform.localPosition = tempPos;
	}
}
