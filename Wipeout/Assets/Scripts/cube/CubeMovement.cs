﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CubeMovement : MonoBehaviour {


    public float acceleration = 5f;
    public float deceleracion = -3f;
    public float maxSpeed = 1.5f;
    public float friction = -1f;
    public float giro = 1f;
    public float gForce = 0f;
    public float minDistanceRight = 1f;
    public float minDistanceLeft = 1f;
	public float coeficienteRocePared = 1f; 

    public LayerMask raycastWith;
    public delegate void CheckPointPass(int n);
    public static event CheckPointPass OnCheckPoint;

    public float factorInterpolacionDireccion = 0.1f; // valor interpolar el forward
	public float factorInterpolacionAltura = 0.05f; // para la altura, suavizar.

    public Text DebugLabel;
    AudioSource AudioToPlay;

    float speed = 0;
    bool accelerando;
    bool frenando; 
    bool girando;
	bool colisonPowerUp = false;
	bool speedBoost = false;
	float tiempoRelentizado=0;
	float tiempoAcelerado=0;


    Vector3 actualRotation;
    Vector3 normalTerrain;
    Vector3 normalWall;
    Vector3 myForward;
    Vector3 direccion;
    float distanceDown;
    float distanceRight;
    float distanceLeft;
    bool canMove = false;
    // Use this for initialization
    void Start () {
        UIController.onPlayerCanMove += setCanMove;
        DestroyByContact.onPlayerhit += setFrenando;
        WingsCollision.onPlayerhit += setFrenando;
        AudioToPlay = GameObject.Find("motor").GetComponent<AudioSource>();
    }
    void OnDestroy()
    {
        UIController.onPlayerCanMove -= setCanMove;
        DestroyByContact.onPlayerhit -= setFrenando;
        WingsCollision.onPlayerhit -= setFrenando;
    }
	// Update is called once per frame
	void Update () {
        if (speed != 0 && !AudioToPlay.isPlaying) AudioToPlay.Play();
        else if(speed == 0 && AudioToPlay.isPlaying) AudioToPlay.Stop();
        HandleMovementKeys();
        recalculatePosition();
        alignNormals();
        HandleMovement();
        recalculatePosition();
    }
    void FixedUpdate ()
    {
        
    }
    void HandleMovementKeys()
    {
        accelerando = false;
        girando = false;
        frenando = false;
        actualRotation = this.transform.localEulerAngles;
        if (canMove)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetAxis("Gatillos") < 0)
            {
                //Debug.Log("Acelerando");
                accelerando = true;
            }
            if (Input.GetKey(KeyCode.S) || Input.GetAxis("Gatillos") > 0)
            {
               // Debug.Log("Frenando");
                frenando = true;
            }
            if (Input.GetKey(KeyCode.A) || Input.GetAxis("Joystic") < 0)
            {
               // Debug.Log("GiroA");
                girando = true;
                if (distanceLeft - giro > minDistanceLeft)
                {
                    this.transform.Translate(Vector3.left * giro*Time.deltaTime*30);
                }
            }
            if (Input.GetKey(KeyCode.D) || Input.GetAxis("Joystic") > 0)
            {
               // Debug.Log("GiroB");
                girando = true;
                if (distanceRight - giro > minDistanceRight)
					this.transform.Translate(Vector3.right * giro*Time.deltaTime*30);
            }
        }

    }

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "PowerUp") {
			colisonPowerUp = true;
			tiempoRelentizado = 1.5f;
		}
		if (other.tag == "speedBoost") {
			speedBoost = true;
			tiempoAcelerado = 1.5f;
		}
	}

    void HandleMovement()
    {
		distanceRight = doRaycastingSideRight();
		distanceLeft = doRaycastingSideLeft();
        myForward = this.transform.forward;
		float colisionPared = 0;
		if (distanceRight < minDistanceRight || distanceLeft < minDistanceLeft)
		{
			colisionPared = coeficienteRocePared;
		}

		if (accelerando) speed += (acceleration-colisionPared) * Time.deltaTime;
		else if (frenando) speed += (deceleracion-colisionPared) * Time.deltaTime;
		else speed += (friction-colisionPared) * Time.deltaTime;
		if (speed < 0)
			speed = 0;
		tiempoAcelerado -= Time.deltaTime;
		if (tiempoAcelerado > 0) {
			if (speed > maxSpeed * 1.25f)
				speed = maxSpeed * 1.25f;
		}
		else if (speed > maxSpeed)
			speed = maxSpeed;
		tiempoRelentizado -= Time.deltaTime;
		if(tiempoRelentizado>0 && speed > maxSpeed/3)
			speed = maxSpeed / 3;
		if (colisonPowerUp) {
			speed = speed * 0.3f;
			colisonPowerUp = false;
		}
		if (speedBoost) {
			speed += acceleration * Time.deltaTime;
		}
		speedBoost = false; 
        Vector3 tempPos = this.transform.position;
        tempPos = tempPos + Vector3.forward * speed;
		this.transform.Translate(Vector3.forward * speed*Time.deltaTime*30);
        
    }
    void alignNormals()
    {
        Vector3 lastDireccion = this.transform.forward;
        direccion = Vector3.Cross(normalTerrain, normalWall);
        direccion = Vector3.Lerp(lastDireccion, direccion, factorInterpolacionDireccion);
        this.transform.forward = direccion;


        float f = Vector3.Angle(direccion, lastDireccion) * Mathf.Sign(Vector3.Cross(direccion, lastDireccion).y);
        if (f > 0)
        {
			this.transform.Translate(Vector3.right *(speed / 10) * gForce*Time.deltaTime*30);
        }
        if (f < -1)
        {
			this.transform.Translate(Vector3.right * (speed / 10) * -gForce*Time.deltaTime*30);
        }
    }
    void recalculatePosition()
    {
        distanceDown = doRaycastingDown();
        distanceRight = doRaycastingSideRight();
        distanceLeft = doRaycastingSideLeft();
        //Todo reposicionar
        if (distanceRight < minDistanceRight)
        {
			this.transform.Translate(Vector3.left*Time.deltaTime*30 * (minDistanceRight-distanceRight));
        }
        if (distanceLeft < minDistanceLeft)
        {
			this.transform.Translate(Vector3.right*Time.deltaTime*30 * (minDistanceLeft-distanceLeft));
        }
        if (distanceDown < 0.5f && distanceDown>0)
        {
			this.transform.Translate(Vector3.up*Time.deltaTime*30 * factorInterpolacionAltura / distanceDown);
        }
        if (distanceDown > 0.8f)
        {
			this.transform.Translate(Vector3.up*Time.deltaTime*30 * -factorInterpolacionAltura * distanceDown);
        }
    }
    float doRaycastingDown()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 50f, raycastWith))
        {
            Debug.DrawRay(this.transform.position, hit.normal * 50, Color.red);
            normalTerrain = hit.normal;
        }
        return hit.distance;
    }
    float doRaycastingSideRight()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, this.transform.TransformDirection(1, 0, 0), out hit, 50f))
        {
            Debug.DrawRay(this.transform.position, hit.normal * 50, Color.cyan);
            normalWall = hit.normal;
        }
        return hit.distance;
    }
    float doRaycastingSideLeft()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, this.transform.TransformDirection(-1,0,0), out hit, 50f))
        {
            Debug.DrawRay(this.transform.position, hit.normal * 50, Color.cyan);
        }
        return hit.distance;
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.name.Equals("RaceStart"))
        {
            OnCheckPoint(0);
        }
        else if (col.gameObject.name.Equals("ChekPoint2"))
        {
            OnCheckPoint(1);
        }
        else if (col.gameObject.name.Equals("ChekPoint3"))
        {
            OnCheckPoint(2);
        }

    }
    public float getSpeed()
    {
        return speed;
    }
    public void setCanMove(bool b)
    {
        canMove = b;
    }
    public void setFrenando(bool b)
    {
        frenando = b;
    }
}
