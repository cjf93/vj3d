﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {


	public Rigidbody rb;
	bool rotating;
    bool lastRotationLeft;

	//public ParticleSystem partSystem;

	public delegate void CheckPointPass(int n);
	public static event CheckPointPass OnCheckPoint;

	Vector3 actualRotation;
	Vector3 prevPosition;
	// Use this for initialization
	void Start () {
		prevPosition = transform.position;
        rotating = false;
	}
	
	// Update is called once per frame
	void Update () {
		HandleMovement();
		//HandleThrustAnim();
        if (!rotating)
        {
            if (this.transform.localEulerAngles.x != 270)
            {
                this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.Euler(270f, this.transform.eulerAngles.y, this.transform.eulerAngles.z), 5f);
            }
        }
	}
    void FixedUpdate()
    {
       
    }
	void OnTriggerEnter(Collider col) {
		if (col.gameObject.name.Equals("CheckPoint1")) {
			OnCheckPoint(1);
		}

	}

	void HandleMovement() {
        rotating = false;
        if (Input.GetKey(KeyCode.W)){

		}
		if (Input.GetKey(KeyCode.S)){

		}
        if (Input.GetKey(KeyCode.A) || Input.GetAxis("Joystic") < 0)
        {
            RotateAnim(false);
            rotating = true;
            lastRotationLeft = true;
		}
        if (Input.GetKey(KeyCode.D) || Input.GetAxis("Joystic") > 0)
        {
            RotateAnim(true);
            rotating = true;
            lastRotationLeft = false;
        }
	}
    void RotateAnim(bool rotatingRight)
    {
        if (rotatingRight)
        {
            if (this.transform.eulerAngles.x <= 340)
            {
                this.transform.Rotate(Vector3.left, 5);
            }
        }
        else
        {
            if (this.transform.eulerAngles.x <= 340)
            {
                this.transform.Rotate(Vector3.right, 5);
            }
        }
    }
	/*void HandleThrustAnim(){
		partSystem.startLifetime = (rb.velocity.magnitude/5)/10 + 0.1f;
		if(partSystem.startLifetime > 0.25f) partSystem.startLifetime = 0.25f;
	}*/
}
