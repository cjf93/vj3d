﻿using UnityEngine;
using System.Collections;

public class PowerUpController : MonoBehaviour {

	public float time=5000f;
	public float currentTime= 5000f;
	public GameObject[] powerUps;
    public GameObject[] powerUpImage;
	private int seleccionado;
	private bool activado = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!activado) currentTime -= Time.deltaTime;
		if (currentTime <=  0f) {
			System.Random rnd = new System.Random ();
			seleccionado = rnd.Next (powerUps.Length);
            powerUpImage[seleccionado].SetActive(true);
            if (seleccionado == 0) { powerUpImage[1].SetActive(false); }
            else if (seleccionado == 1) { powerUpImage[0].SetActive(false); }
			currentTime = time;
			activado = true;
		}
		HandleMovementKeys ();
	}

	void HandleMovementKeys()
	{
		if (Input.GetKey(KeyCode.Space) && activado)
		{
			Vector3 posicion= new Vector3(0,0,0);
			Quaternion rotation = new Quaternion();
			activado = false;
			switch (powerUps[seleccionado].name){
			case "misil":
				posicion = this.transform.position + Vector3.Normalize (this.transform.forward) * 10f;
				rotation = this.transform.rotation * Quaternion.Euler(0,180f,0);
				break;
			case "mina":
				posicion = this.transform.position + Vector3.Normalize (this.transform.forward) * -2f;
				rotation = this.transform.rotation;
				break;
			default:
				break;
			}
			Instantiate (powerUps [seleccionado], posicion, rotation);
            powerUpImage[seleccionado].SetActive(false);
		}
	}


}
