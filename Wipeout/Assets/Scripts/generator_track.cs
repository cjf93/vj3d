﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace vj
{


	public class generator_track : MonoBehaviour
	{

		public int elevacion;
		public int distancia = 5;
		public GameObject[] prefabs;
		private Road[] piezas;
		private Vector3 posicionActual;
		private direccion direc_actual;
		public int seed;




		void leerPrefabs ()
		{
			int i = 0;
			piezas = new Road [prefabs.Length];
			foreach (GameObject p in prefabs) {
				Debug.Log ("Salida:" + p.transform.Find ("salida").transform.position);
				Road road = new Road (
					            new Vector3 (0, 0, 0),
					            p.transform.Find ("salida").transform.position,
					            i,
					            p.GetComponent<RoadInfo>().direccionEntrada,
					            p.GetComponent<RoadInfo> ().direccionSalida
				            );
				piezas [i] = road;
				++i;
			}
		}

		void instanciarTrack ()
		{
			posicionActual = new Vector3 (0, 0, 0); 
			Instantiate (prefabs [0], posicionActual, prefabs [0].GetComponent<Transform> ().rotation);
			posicionActual = prefabs [0].transform.Find ("salida").transform.position;
			direc_actual = prefabs [0].GetComponent<RoadInfo> ().direccionSalida;
		}

		int siguiente_pieza ()
		{
			List<int> candidatos = new List<int> ();
			int i = 0;
			foreach (Road p in piezas) {
				if (p.Direc_entrada == direc_actual)
					candidatos.Add (i);
				++i;
			}
			System.Random rnd = new System.Random ();
			//
			return candidatos [rnd.Next (candidatos.Count)];
		}

		// Use this for initialization
		void Start ()
		{
			leerPrefabs ();
			Vector3 inicio = new Vector3 (0, 0, 0);
			instanciarTrack ();
		}
		
		// Update is called once per frame
		void Update ()
		{
			if (distancia >= 0) {
				int i = siguiente_pieza ();
				Instantiate (prefabs [i], posicionActual, prefabs [i].GetComponent<Transform> ().rotation);
				posicionActual += prefabs [i].transform.Find ("salida").transform.position;
				direc_actual = prefabs [i].GetComponent<RoadInfo> ().direccionSalida;
				--distancia;
			}
		}
	}
}

